module.exports = {
  plugins: {
    'stylelint': {},
    'precss': {},
    'lost': {},
    'rucksack-css': {},
    'postcss-cssnext': {
      browsers: [
        'last 2 versions',
        'ie >= 11'
      ]
    },
    'postcss-em-media-query': {},
    'postcss-assets': {},
    'postcss-at2x': {},
    'css-mqpacker': {
      sort: true
    },
    'postcss-reporter': {
      clearMessages: true
    }
  }
};
