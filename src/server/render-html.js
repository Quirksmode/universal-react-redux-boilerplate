import React from 'react';
import { Helmet } from 'react-helmet';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import serialize from 'serialize-javascript';
import Routes from '../universal/Routes';

export default (req, store, context) => {
  const content = renderToString(
    <Provider store={ store }>
      <StaticRouter location={ req.path } context={ context }>
        <div>{renderRoutes(Routes)}</div>
      </StaticRouter>
    </Provider>);

  const helmet = Helmet.renderStatic();

  return `
    <html className="no-js">
      <head>
        ${ helmet.title.toString() }
        ${ helmet.meta.toString() }
        <script>let docElement=document.documentElement;docElement.className=docElement.className.replace(/\bno-js\b/, '') + ' js';let isTouch = 'ontouchstart' in document.documentElement;isTouch?docElement.classList.add('touch'):docElement.classList.add('no-touch');</script>
        <link rel="stylesheet" href="/style.css">
      </head>
      <body>
        <div id="root">${ content }</div>
        <script>
          window.INITIAL_STATE = ${ serialize(store.getState()) }
        </script>
        <script src="/build-client.js"></script>
      </body>
    </html>
  `;
};
