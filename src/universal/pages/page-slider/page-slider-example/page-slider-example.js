import React from 'react';
import Swiper from 'react-id-swiper';
import PropTypes from 'prop-types';

const PageSliderExample = (props) => {
  const params = {
    slidesPerView: 3,
    spaceBetween: 50,
    rebuildOnUpdate: true,
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      481: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  };

  return (
    <section className="page-slider-example">
      <h2>Asyncronous Data Example</h2>
      <Swiper { ...params }>
        {
          props.images.slice(0, 10).map((item) => {
            return (
              <div className="page-slider-example__slide" key={ item.id }>
                <div>
                  <img src={ item.thumbnailUrl } alt="" />
                  <p>{ item.title }</p>
                </div>
              </div>
            );
          })
        }
      </Swiper>
    </section>
  );
};

PageSliderExample.propTypes = {
  images: PropTypes.array
};

export default PageSliderExample;
