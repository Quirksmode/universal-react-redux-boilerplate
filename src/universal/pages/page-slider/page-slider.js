import { Helmet } from 'react-helmet';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchData } from './page-slider.actions';
import PageSliderExample from './page-slider-example/page-slider-example';

class PageSlider extends Component {
  componentDidMount() {
    this.props.fetchData();
  }

  head() {
    return (
      <Helmet>
        <title>Slider Page</title>
        <meta property="og:title" content="Slider Page" />
      </Helmet>
    );
  }

  render() {
    return (
      <main className="page page-slider">
        {this.head()}
        <h1>Slider Example</h1>
        <PageSliderExample images={ this.props.pageSlider.images } />
      </main>
    );
  }
}

PageSlider.propTypes = {
  fetchData: PropTypes.func,
  pageSlider: PropTypes.object
};

function mapStateToProps(state) {
  return { pageSlider: state.pageSlider };
}

function loadData(store) {
  return store.dispatch(fetchData());
}

export { loadData };
export default {
  loadData,
  component: connect(mapStateToProps, { fetchData })(PageSlider)
};
