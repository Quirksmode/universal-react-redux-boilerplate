export const FETCH_DATA = 'fetch_data';
export const fetchData = () => async (dispatch, getState, api) => {
  const res = await api.get('photos');
  dispatch({
    type: FETCH_DATA,
    payload: res
  });
};
