import { FETCH_DATA } from './page-slider.actions';

const initialState = {
  images: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA:
      return {
        ...state,
        images: action.payload.data
      };
    default:
      return state;
  }
};
