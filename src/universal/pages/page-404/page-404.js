import { Helmet } from 'react-helmet';
import React, { Component } from 'react';

class Page404 extends Component {
  head() {
    return (
      <Helmet>
        <title>404 Page</title>
        <meta property="og:title" content="404 Page" />
      </Helmet>
    );
  }

  render() {
    return (
      <main className="page page-404">
        {this.head()}
        <h1>404 Page</h1>
      </main>
    );
  }
}

export default {
  component: Page404
};
