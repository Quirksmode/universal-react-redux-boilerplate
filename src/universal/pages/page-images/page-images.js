import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import SVGInline from 'react-svg-inline';
import IconMenu from 'icons/menu.svg';
import IconSearch from 'icons/search.svg';
import IconTwitter from 'icons/twitter.svg';
import IconFacebook from 'icons/facebook.svg';

class PageImages extends Component {
  head() {
    return (
      <Helmet>
        <title>Images Page</title>
        <meta property="og:title" content="About Page" />
      </Helmet>
    );
  }

  render() {
    return (
      <main className="page page-images">
        {this.head()}
        <h1>Image Examples</h1>
        <h2>SVG</h2>
        <SVGInline svg={ IconMenu } />
        <SVGInline svg={ IconSearch } />
        <SVGInline svg={ IconTwitter } />
        <SVGInline svg={ IconFacebook } />
      </main>
    );
  }
}

export default {
  component: PageImages
};
