import { Helmet } from 'react-helmet';
import React, { Component } from 'react';

class PageHome extends Component {
  head() {
    return (
      <Helmet>
        <title>Home Page</title>
        <meta property="og:title" content="Home Page" />
      </Helmet>
    );
  }

  render() {
    return (
      <main className="page page-home">
        {this.head()}
        <h1>Universal React Boilerplate</h1>
        <p>Welcome to this Boilerplate...</p>
      </main>
    );
  }
}

export default {
  component: PageHome
};
