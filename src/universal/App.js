import React from 'react';
import { renderRoutes } from 'react-router-config';
import PropTypes from 'prop-types';
import Header from 'components/header/header';
import Footer from 'components/footer/footer';
import AccessibilityLinks from 'components/accessibility-links/accessibility-links';
import '../assets/css/styles.css';

const App = ({ route }) => {
  return (
    <div>
      <AccessibilityLinks />
      <Header />
      <p className="visually-hidden" id="int_content">
        <strong>Main Content</strong>
      </p>
      {renderRoutes(route.routes)}
      <Footer />
    </div>
  );
};

App.propTypes = {
  route: PropTypes.object
};

export default {
  component: App
};
