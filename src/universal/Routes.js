import PageHome from 'pages/page-home/page-home';
import PageImages from 'pages/page-images/page-images';
import PageSlider from 'pages/page-slider/page-slider';
import Page404 from 'pages/page-404/page-404';
import App from './App';

export default [
  {
    ...App,
    routes: [
      {
        ...PageHome,
        path: '/',
        exact: true
      },
      {
        ...PageImages,
        path: '/images'
      },
      {
        ...PageSlider,
        path: '/slider'
      },
      {
        ...Page404
      }
    ]
  }
];
