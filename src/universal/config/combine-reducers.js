import { combineReducers } from 'redux';
import pageSliderReducer from '../pages/page-slider/page-slider.reducer';

export default combineReducers({
  pageSlider: pageSliderReducer
});
