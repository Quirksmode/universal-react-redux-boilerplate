import React from 'react';
import Nav from './nav/nav';

const Header = () => {
  return (
    <header className="header clearfix">
      <Nav />
    </header>
  );
};

export default Header;
