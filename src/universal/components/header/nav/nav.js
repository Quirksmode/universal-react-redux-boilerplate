import React from 'react';
import { NavLink } from 'react-router-dom';

const Nav = () => {
  return (
    <nav className="nav">
      <ul className="nav__list">
        <li>
          <NavLink
            exact
            activeClassName="selected"
            to="/"
          >
            Home
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            to="/images"
          >
            Images
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            to="/slider"
          >
            Slider
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
