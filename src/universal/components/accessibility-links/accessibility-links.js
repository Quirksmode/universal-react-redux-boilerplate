import React from 'react';

const AccessibilityLinks = () => {
  return (
    <div className="accessibility-links visually-hidden">
      <p id="int_top">
        <strong>Accessibility Links</strong>
      </p>
      <ul>
        <li>
          <a href="#int_content">Skip to Main Content</a>
        </li>
        <li>
          <a href="#int_footer">Skip to Footer Content</a>
        </li>
      </ul>
    </div>
  );
};

export default AccessibilityLinks;
