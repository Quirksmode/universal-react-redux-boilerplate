const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const { getIfUtils, removeEmpty } = require('webpack-config-utils');

const { ifProduction, ifNotProduction } = getIfUtils(process.env.NODE_ENV);

const config = {
  name: 'client',
  target: 'web',

  entry: './src/client/index.js',

  output: {
    filename: 'build-client.js',
    path: path.resolve(__dirname, 'build/public')
  },

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },

  plugins: removeEmpty([
    new CleanWebpackPlugin(['build']),
    new MiniCssExtractPlugin({
      filename: 'style.css'
    }),
    ifNotProduction(new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      filename: path.resolve(__dirname, 'build/public/index.html')
    }))
  ]),

  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          ifProduction(MiniCssExtractPlugin.loader, 'style-loader'),
          'css-loader',
          'postcss-loader'
        ]
      },
      {
        test: /\.(png|jp(e*)g|gif)$/,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      }
    ]
  },

  devServer: {
    contentBase: path.resolve(__dirname, 'build/public'),
    historyApiFallback: true,
    port: 3000,
    compress: false,
    inline: true,
    hot: true,
    host: 'localhost',
    overlay: {
      warnings: true,
      errors: true
    }
  }
};

module.exports = merge(baseConfig, config);
