# Universal React Redux Boilerplate ★★

Universal React, Redux, Webpack, Node, Express boilerplate.

## Features
- Universal React JS
- Redux for managing state
- Webpack 4
- Post CSS (BEM)
- ESLint
- StyleLint
- Slider Example
- Inline SVG Example
- Hot Reloading
- Modularised Component Structure

## Installation

```bash
npm install
```

Run Development (Builds and runs Web Dev Server with Hot Reloading)
```bash
npm run dev
```

Build Production (Generates Minified bundles ready for deployment)
```bash
npm run prod
```

## To Do
- Inline critical CSS
- Optimise JS Chunk Splitting
- PWA
- Commenting and Auto Doc generation
- Accessibility review
- Generate Static
